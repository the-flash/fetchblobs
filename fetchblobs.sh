#/bin/sh
# Author: ErfanoAbdi
# https://github.com/erfanoabdi/
proptxt="$1"
URL="$2"
dirdis="$3"
while read -r line
do
    [[ $line = \#* ]] && continue
    [ -z "$line" ] && continue
    if [[ $line = '-'* ]]
    then
    line=`echo "$line" | cut -c2-`
    fi

    filesrc=`echo "$line" | cut -d ":" -f 1 | cut -d "|" -f 1`
    filedis=`echo "$line" | cut -d ":" -f 2 | cut -d "|" -f 1`
    filedir=`echo $filedis | rev | cut -d "/" -f 2- | rev`
    mkdir -p "$dirdis/$filedir"
    wget -q "$URL/$filesrc" -O "$dirdis/$filesrc"
done < "$proptxt"
