#/bin/sh
# Author: ErfanoAbdi
# https://github.com/erfanoabdi/

if [ $1 ] || [ $2 ] || [ $3 ]; then
proptxt="$1"
URL="$2"
dirdis="$3"
while read -r line
do
    [[ $line = \#* ]] && continue
    [ -z "$line" ] && continue
    if [[ $line = '-'* ]]
    then
    line=`echo "$line" | cut -c2-`
    fi

    filesrc=`echo "$line" | cut -d ":" -f 1 | cut -d "|" -f 1`
    filedis=`echo "$line" | cut -d ":" -f 2 | cut -d "|" -f 1`
    filedir=`echo $filedis | rev | cut -d "/" -f 2- | rev`
    mkdir -p "$dirdis/$filedir"
    wget -q "$URL/$filesrc" -O "$dirdis/$filesrc"
done < "$proptxt"
else
	echo -e "syntax: ./mkflashzip.sh <fetch-propfiles.txt> <https://github.com/motoe5/motorola_james_dump/tree/user-8.0.0-OCPS27.91-140-10-10-release-keys> <description>"
	exit
fi
cp -rav META-INF $dirdis/
cd $dirdis
makescript() {
UPDATERSCRIPT="META-INF/com/google/android/updater-script"
echo "ui_print("Test blobs for $dirdis");" >> $UPDATERSCRIPT
echo "ui_print("source: $URL");" >> $UPDATERSCRIPT
echo "ui_print("Flashable zip script by @F1a5H");" >> $UPDATERSCRIPT
echo "ui_print("fetchblobs.sh by @erfanoabdi");" >> $UPDATERSCRIPT
echo "show_progress(1.00000, 10);" >> $UPDATERSCRIPT
if [ -d system ]; then
	echo "ifelse(is_mounted("/system"), unmount("/system"));" >> $UPDATERSCRIPT
	echo "ui_print("Mounting system...");"
#	echo "mount("ext4", "EMMC", "/dev/block/bootdevice/by-name/system", "/system");" >> $UPDATERSCRIPT
	echo "mount("ext4", "EMMC", "/dev/block/bootdevice/by-name/system", "/system_root");" >> $UPDATERSCRIPT
	echo "ui_print("Extracting files...");" >> $UPDATERSCRIPT
	echo "package_extract_dir("system", "/system");" >> $UPDATERSCRIPT
	for i in `find system -type f`;do echo "set_perm(0, 0, 0644, "\"$i\"");" >> $UPDATERSCRIPT;done
	cat $UPDATERSCRIPT|grep -iv system/bin > updater-new
	mv updater-new $UPDATERSCRIPT
	if [ -d system/bin ]; then
	        for i in `find system/bin -type f`;do echo "set_perm(0, 0, 0755, "\"$i\"");" >> $UPDATERSCRIPT;done
	fi
	echo "unmount("/system");" >> $UPDATERSCRIPT
fi
if [ -d vendor ]; then
	echo "ifelse(is_mounted("/vendor"), unmount("/vendor"));" >> $UPDATERSCRIPT
	echo "ui_print("Mounting vendor...");" >> $UPDATERSCRIPT
	echo "mount("ext4", "EMMC", "/dev/block/bootdevice/by-name/vendor", "/vendor");" >> $UPDATERSCRIPT
	echo "ui_print("Extracting files...");" >> $UPDATERSCRIPT
	echo "package_extract_dir("vendor", "/vendor");" >> $UPDATERSCRIPT
	for i in `find vendor -type f`;do echo "set_perm(0, 0, 0644, "\"$i\"");" >> $UPDATERSCRIPT;done
	cat $UPDATERSCRIPT|grep -iv vendor/bin > updater-new
	mv updater-new $UPDATERSCRIPT
	if [ -d vendor/bin ]; then
	        for i in `find vendor/bin -type f`;do echo "set_perm(0, 0, 0755, "\"$i\"");" >> $UPDATERSCRIPT;done
	fi
	fixpermven
	echo "unmount("/vendor");" >> $UPDATERSCRIPT
fi
echo "ui_print("Installation complete!");" >> $UPDATERSCRIPT
}
makescript
zip ./$dirdis.zip -r META-INF system/ vendor/
