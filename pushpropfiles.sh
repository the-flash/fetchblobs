#!/bin/bash
# Author: ErfanoAbdi
# https://github.com/erfanoabdi/

proptxt="$1"
dirsrc="$2"
while read -r line
do
	[[ $line = \#* ]] && continue
	[ -z "$line" ] && continue
	if [[ $line = '-'* ]]
	then
		line=`echo "$line" | cut -c2-`
	fi

	filesrc=`echo "$line" | cut -d ":" -f 1 | cut -d "|" -f 1`
	filedis=`echo "$line" | cut -d ":" -f 2 | cut -d "|" -f 1`
	filedir=`echo $filedis | rev | cut -d "/" -f 2- | rev`
	adb push "$dirsrc/$filesrc" "/$filedir/"
done < "$proptxt"
